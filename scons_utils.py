from sys import exit
from subprocess import check_output
from re import search
from multiprocessing import cpu_count

def get_concurrency_flag():
	return "-j%d" % cpu_count()

class Version_Check:
	"""Just to check the gcc version"""
	def __init__(self, compiler):
		self.compiler_to_use = None
		self.compilers = ["g++", "g++-5", "g++-6"]

		if compiler:
			self.compilers = compiler.split(",") + self.compilers

		self.min_version = "5.0.0"
		colors = {
			"header" : "\033[1;94m",
			"color": "\033[0;94m",
			"normal": "\033[0;39m",
			"warning" : "\033[1;91m",
			"ok": "\033[92m"
			 }
		self.colors = colors
		self.msj = {
			"start" : "{}Checking the version of GCC on your system.{}"\
														.format(colors["header"],colors["color"]),
			"checking" : "Trying {} ..",
			"not_found" : "Compiler {} not found on your system.",
			"ok" : "{}OK, starting compilation.{}"\
														.format(colors["ok"],colors["color"]),
			"error" : "{}ERROR, found GCC version {{}}{}"\
														.format(colors["warning"],colors["color"]),
			"unknown" : "{}Can't get the version of GCC on your system{}"\
														.format(colors["warning"],colors["color"]),
			"lower" : "The gcc version on your system can't be used to compile this program",
			"final" : "In order to compile Notochord you need GCC version 5 or higher",
			"prompt": "Continue anyway using [{}] as the compiler? (y/n) ",
			"please" : "please answer: y/n",
			"abort" : "{}aborting compilation{}".format(colors["warning"],colors["color"])
			}
		self.valid_answ = {"yes":("y", "yes", "ok", "go"), "no" : ("n", "no", "nop", "nain")}



	def run_check(self):
		print self.msj["start"]

		for c in self.compilers:
			print self.msj["checking"].format(c)

			res = self.check_gcc_version(c)
			if res:
				print self.msj["ok"]
				print self.colors["normal"]
				return self.compiler_to_use

			else:
				try:
					if self.last_v != "0.0.0":
						print self.msj["error"].format(self.last_v)

				except NameError:
					print self.msj["error"].format("UNKNOWN, error in verification procedure")
				
		print self.msj["lower"]
		print self.msj["final"]
		print self.colors["normal"]

		exit()


	def get_gcc_version(self, compiler):
		try:
			version = check_output([compiler , '--version'])
			
		except OSError as e:
			print self.msj["not_found"].format(compiler)
			return "0.0.0"

		version_num = search('\d\.\d\.\d', version)

		if version_num:
			return version_num.group(0)
		else:
			return False

	def check_gcc_version(self, compiler):
		v = self.get_gcc_version(compiler)
		if v:
			self.last_v = v
			if v > self.min_version:
				self.compiler_to_use = compiler
				return True
			else:
				return False


		else:
			print self.msj["unknown"]
			print self.msj["final"]
			
			answ = raw_input(self.msj["prompt"].format(compiler))
			while answ not in self.valid_answ["yes"] + self.valid_answ["no"]:
				answ = raw_input(self.msj["please"])

			if answ in self.valid_answ["no"]:
				print self.msj["abort"]
				exit()

			self.compiler_to_use = compiler
			return True