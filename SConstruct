"""
Scons construction file for Notochord.
Hub software of the Chordata motion capture system

"""
import scons_utils as utils
from os import mkdir
from os import environ
from os import popen
from shutil import copyfile



cmd_line_compiler =  ARGUMENTS.get('compiler', 0)
vc = utils.Version_Check(cmd_line_compiler)
git_hash = popen('git rev-parse HEAD').read()

vars = Variables(None, ARGUMENTS)
vars.Add('compiler', 'Pass the name or path of the compiler to be used\n A comma separated list can also be passed ', vc.compilers, 0)
vars.Add('debug', 'Set to 1 in order make a debug compilation ( -g flag ) ', 0)
# vars.Add('i2c_support', 'Set to 0 in order make dummy compilation with no real i2c ', 1)
vars.Add('test', "Set to 1 in order to compile the test suit", 0)
vars.Add('permisive', "Set to 1 in order to compile the test with -fpermissive", 0)


if not GetOption('help'):
	compiler = vc.run_check()

	env = Environment(
		CXX= compiler,
		CCFLAGS= ['-std=c++14', "-pthread", "-w", "-Wno-deprecated"],
		CPPPATH=['#lib', '#src', '#lib/oscpack_1_1_0','#lib/MadgwickAHRS', '#lib/tinyxml2'],
		CPPDEFINES = [("_CHORDATA_GIT_HASH", '\\"{}\\"'.format(git_hash[:7]))]
		)

	# Pass the terminal to the environment in order to let Scons check if it's color-capable
	try:
		env['ENV']['TERM'] = environ['TERM']
	except Exception as e:
		print "Couldn't determinate $TERM env variable"
	# if not GetOption('i2c_support'):
	# 		env.Append(CPPDEFINES= ["__NO_I2C_SUPPORT__"])

	debug = ARGUMENTS.get('debug', 0)
	if int(debug):
		env.Append(CCFLAGS = ['-g'])

	_chordlib = SConscript(['src/SConscript'], exports = 'env')

	_libs = _chordlib + SConscript(['lib/SConscript'], exports = 'env')

	test = ARGUMENTS.get("test")
	if not test:
		env.Append(	CPPDEFINES = ["__CHORDATA_MAIN__"])
		try:
			mkdir("bin")
		except Exception as e:
			pass

		copyfile("calib/getMagCalib.m", "bin/getMagCalib.m")

		env.Append(CPPPATH = ["/usr/include/octave-4.0.0/octave", 
			 "/usr/include/octave-4.0.3/octave", 
			 "/usr/include/octave-4.2.2/octave",
			 "/usr/include/octave-4.4.1/octave"])

		env.Program("#bin/notochord", "src/Notochord.cpp",
				LIBPATH =  ["/usr/lib/x86_64-linux-gnu", "/usr/lib/arm-linux-gnueabihf"],
				LIBS = _libs + ["pthread", "octinterp", "octave"]
				)
	else:
		env.Append(CPPPATH = ["#test"])
		env.Append(CCFLAGS = ["-pthread"])
		env.Append(CPPDEFINES = ["_CHORDATA_TEST_BUILD__"])
		if GetOption('help'):
			env.Append(CCFLAGS = ["-fpermissive"])
			 
		env.Program("#test/Chordata_test" , Glob("test/Chordata_test*.cpp") + ["src/Notochord.cpp"],
			LIBS = _libs + ["pthread"]
			)

		for log in Glob("test/logs/*"):
			log.remove()

env = Environment(variables = vars)
Help("\nScons Help \n for Notochord, Hub software of the Chordata motion capture system.\n")
Help(vars.GenerateHelpText(env))
