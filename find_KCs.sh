#!/bin/bash

MUX_ADDR=0x73
LSM9DS1_GYRO_ADD=0x1e

if [ ! -z $1 ]
then
	MUX_ADDR=$1
fi


i2cget -y 1 $MUX_ADDR 0 > /dev/null
if [ ! $? -eq 0 ]
then
	echo MUX not found at $MUX_ADDR
	echo exiting..
	exit 1
fi


echo MUX found at $MUX_ADDR

for N in 1 2 3 4 5 6
do
	echo -en "== K-Ceptors in gate $N ==> "  
	GATE_VAL=$((1 << $N -1 ))
	i2cset -y 1 $MUX_ADDR $GATE_VAL 
	for TRANS_VAL in {0..15}
	do
	   i2cget -y 1 $(( $LSM9DS1_GYRO_ADD ^ $TRANS_VAL )) >/dev/null 2> /dev/null
	   if [ $? -eq 0 ]
	   then
	   	echo -n [$TRANS_VAL] 
	   fi
	done
	echo

done

exit 0