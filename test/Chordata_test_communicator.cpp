/**
 * @file Chordata_test_communicator.cpp
 * Testing the communicator namespace.
 *
 * @author Bruno Laurencich
 * @version 1.0 
 * @date 2017/10/29
 *
 * Notochord  
 * -- Hub program for the Chordata Open Source motion capture system
 *
 * http://chordata.cc
 * contact@chordata.cc
 *
 *
 * Copyright 2018 Bruno Laurencich
 *
 * This file is part of Notochord.
 *
 * Notochord is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Notochord is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Notochord. If not, see <https://www.gnu.org/licenses/>.
 *
 * This program uses code from various sources, the default license is GNU GPLv3
 * for all code, the dependencies where originally distributed as follows:
 * -LSM9DS# libraries: Copyright 2014,2015 Jim Lindblom @ SparkFun Electronics, as beerware.
 * -fmt library: Copyright (c) 2012 - present, Victor Zverovich, Under BSD 2-clause License.
 * -spdlog library: Copyright(c) 2015 Gabi Melman, under MIT license.
 * -args library: Copyright (c) 2016 Taylor C. Richberger, under MIT license.
 * -bcm2835 library: Copyright 2011-2013 Mike McCaule, Under GNU-GPLv2 License.
 * -oscpack library: Copyright (c) 2004-2013 Ross Bencina, under MIT license.
 * -Sensor fusion algorithms: Copyright 2011 SOH Madgwick, Under GNU-GPLv3 License.
 * -tinyxml2 library: Copyright <unknowk year> Lee Thomason, under zlib License.
 * -pstreams library: Copyright (C) 2001 - 2017 Jonathan Wakely, under Boost Software License
 * -catch library: Copyright (c) 2012 Two Blue Cubes Ltd, under the Boost Software License, Version 1.0
 * -trompeloeil mocking framework: Copyright Björn Fahller 2014-2017 under the Boost Software License, Version 1.0
 *
 */

#include "catch.hpp"
#include "trompeloeil.hpp"
#include "Chordata_communicator.h"
#include "Chordata_utils.h"
#include "Chordata_test.h"

// #include <system_error>
#include <osc/OscReceivedElements.h>
#include <osc/OscPacketListener.h>
// #include <future>

using Catch::Matchers::Contains;
using Catch::Matchers::Equals;

using trompeloeil::_;

extern template struct trompeloeil::reporter<trompeloeil::specialized>;



//////////////////////////////////
/// TODO 
//////////////////////////////////

TEST_CASE("TODO communicator", "[.TODO]"){
	WARN("Refactor Chordata::Communicator::initializers functions"
		", collapse them all on a single template");

	WARN("Test the OSC communications on other redirects");

	WARN("Create catching bad_function_call MACROS");

	WARN("expose namespace functions to call the timekeeper instance");

	WARN("Trasmit stdout (and perhaps others) is not working when trasmit osc");


}


//////////////////////////////////
/// TEST CASES 
//////////////////////////////////

#define _TEMP_DIR "u3543fjs9230iJH3i"


TEST_CASE("temp", "[.temp]"){

	try{
	throw Chordata::System_Error(24, "testiame");

	} catch (const Chordata::System_Error& e){
		std::cout << e.code() << std::endl;
		std::cout << e.what() << std::endl;
	}

}

TEST_CASE("Directory creation and getting exe path","[utils]"){
	
	if ( -1 == Chordata::create_dir(_TEMP_DIR)  )
		FAIL("Error, cannot create directory");

	CHECK(system("rm -rf " _TEMP_DIR) != -1);

	REQUIRE_THAT(Chordata::get_exe_path(), Contains("/"));

	// std::cout << "DIR: " << Chordata::get_exe_path() << std::endl;
}


SCENARIO("The init_communicator function is a one shot interface to set all the communicators","[comm][config]"){
	GIVEN("A Configuration_Data with an arbitrary combination of redirects"){
		auto d = Chordata_test::create_config_for_OSC();

		WHEN("A communications subtrate is constructed from that configuration data"){
			Chordata::Communicator::Comm_Substrate cConf(d);

			THEN("It should create vectors containing pointers to the corresponding classes"){
				using namespace Chordata::Communicator;

				CHECK(cConf.log.size() == 2);
		
				CHECK( typeid( *(cConf.log[0])) == typeid(spdlog::sinks::ansicolor_stderr_sink_mt));
				CHECK( typeid( *(cConf.log[1])) == typeid(spdlog::sinks::daily_file_sink_mt));
		
				CHECK( typeid( *(cConf.error[0])) == typeid(spdlog::sinks::daily_file_sink_mt));
		
				CHECK(std::dynamic_pointer_cast<Chordata::Communicator::OscOut>(cConf.transmit[0]) == nullptr);
				CHECK(std::dynamic_pointer_cast<Chordata::Communicator::OscOut>(cConf.transmit[1]) != nullptr);

				WHEN("A user calls init_communicator()"){
					THEN("It should set the global communications functions"){
						REQUIRE_NOTHROW( init_communicator( std::move(cConf) ));

						CHECK_NOTHROW(Chordata::Communicator::debug("debug test (replicated on stderr and file)"));
						CHECK_NOTHROW(Chordata::Communicator::info("info test (replicated on stderr and file)"));
						CHECK_NOTHROW(Chordata::Communicator::warn("warning test"));
						CHECK_NOTHROW(Chordata::Communicator::err("error test"));

						CHECK_NOTHROW(Chordata::Communicator::info("A message containing fmt::format entry = {} .. {:>2b}", 43, 16));						

						WHEN("Some communication function with a OSC redirect is called"){
							THEN("An OSC message should be sent"){

								//Wait for the OSC server `ready` msg..
								char buf;
								while (read(Chordata_test::pipefd[0], &buf, 1) > 0 && buf != '\n'){}

								//Send a test
								const char* message = "output_test";
								Chordata::Communicator::transmit(message);

								//Wait for the response
								fmt::MemoryWriter w;
								while (read(Chordata_test::pipefd[0], &buf, 1) > 0 && buf != '\n') 
						               w << buf;

					        	CHECK_THAT(w.c_str(), Contains(message));
					        	REQUIRE_THAT(w.c_str(), !Contains("Timeout") && !Contains("Too much arguments"));

					        	message = "transmit test";
								Chordata::Communicator::transmit("transmit test", Quaternion(0,1,2,3));

								//Wait for the response
								w.clear();
								while (read(Chordata_test::pipefd[0], &buf, 1) > 0 && buf != '\n') 
						               w << buf;

					        	CHECK_THAT(w.c_str(), Contains(message) && !Contains("Too much arguments"));
					        	REQUIRE_THAT(w.c_str(), Contains("1") && Contains("2") && Contains("3"));
								
								
							}
						}
					}
				}
			}
		}
	}
}