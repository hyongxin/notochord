// DO NOT EDIT!  Generated automatically from oct-conf.in.h by Make.
/*

Copyright (C) 1996-2015 John W. Eaton

This file is part of Octave.

Octave is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 3 of the License, or (at your
option) any later version.

Octave is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with Octave; see the file COPYING.  If not, see
<http://www.gnu.org/licenses/>.

*/

#if !defined (octave_conf_h)
#define octave_conf_h 1

#ifndef OCTAVE_CONF_ALL_CFLAGS
#define OCTAVE_CONF_ALL_CFLAGS ""
#endif

#ifndef OCTAVE_CONF_ALL_CXXFLAGS
#define OCTAVE_CONF_ALL_CXXFLAGS ""
#endif

#ifndef OCTAVE_CONF_ALL_FFLAGS
#define OCTAVE_CONF_ALL_FFLAGS "-g -O2 -fdebug-prefix-map=/build/octave-tfO910/octave-4.0.3=. -fstack-protector-strong"
#endif

#ifndef OCTAVE_CONF_ALL_LDFLAGS
#define OCTAVE_CONF_ALL_LDFLAGS ""
#endif

#ifndef OCTAVE_CONF_AMD_CPPFLAGS
#define OCTAVE_CONF_AMD_CPPFLAGS ""
#endif

#ifndef OCTAVE_CONF_AMD_LDFLAGS
#define OCTAVE_CONF_AMD_LDFLAGS ""
#endif

#ifndef OCTAVE_CONF_AMD_LIBS
#define OCTAVE_CONF_AMD_LIBS "-lamd"
#endif

#ifndef OCTAVE_CONF_ARFLAGS
#define OCTAVE_CONF_ARFLAGS "cr"
#endif

#ifndef OCTAVE_CONF_AR
#define OCTAVE_CONF_AR "ar"
#endif

#ifndef OCTAVE_CONF_ARPACK_CPPFLAGS
#define OCTAVE_CONF_ARPACK_CPPFLAGS ""
#endif

#ifndef OCTAVE_CONF_ARPACK_LDFLAGS
#define OCTAVE_CONF_ARPACK_LDFLAGS ""
#endif

#ifndef OCTAVE_CONF_ARPACK_LIBS
#define OCTAVE_CONF_ARPACK_LIBS "-larpack"
#endif

#ifndef OCTAVE_CONF_BLAS_LIBS
#define OCTAVE_CONF_BLAS_LIBS "-lblas"
#endif

#ifndef OCTAVE_CONF_CAMD_CPPFLAGS
#define OCTAVE_CONF_CAMD_CPPFLAGS ""
#endif

#ifndef OCTAVE_CONF_CAMD_LDFLAGS
#define OCTAVE_CONF_CAMD_LDFLAGS ""
#endif

#ifndef OCTAVE_CONF_CAMD_LIBS
#define OCTAVE_CONF_CAMD_LIBS "-lcamd"
#endif

#ifndef OCTAVE_CONF_CARBON_LIBS
#define OCTAVE_CONF_CARBON_LIBS ""
#endif

#ifndef OCTAVE_CONF_CC
#define OCTAVE_CONF_CC "gcc"
#endif

// FIXME: OCTAVE_CONF_CCC_VERSION is deprecated.  Remove in version 3.12
#ifndef OCTAVE_CONF_CC_VERSION
#define OCTAVE_CONF_CC_VERSION %OCTAVE_CONF_CC_VERSION%
#endif

#ifndef OCTAVE_CONF_CCOLAMD_CPPFLAGS
#define OCTAVE_CONF_CCOLAMD_CPPFLAGS ""
#endif

#ifndef OCTAVE_CONF_CCOLAMD_LDFLAGS
#define OCTAVE_CONF_CCOLAMD_LDFLAGS ""
#endif

#ifndef OCTAVE_CONF_CCOLAMD_LIBS
#define OCTAVE_CONF_CCOLAMD_LIBS "-lccolamd"
#endif

#ifndef OCTAVE_CONF_CFLAGS
#define OCTAVE_CONF_CFLAGS "-g -O2 -fdebug-prefix-map=/build/octave-tfO910/octave-4.0.3=. -fstack-protector-strong -Wformat -Werror=format-security"
#endif

#ifndef OCTAVE_CONF_CHOLMOD_CPPFLAGS
#define OCTAVE_CONF_CHOLMOD_CPPFLAGS ""
#endif

#ifndef OCTAVE_CONF_CHOLMOD_LDFLAGS
#define OCTAVE_CONF_CHOLMOD_LDFLAGS ""
#endif

#ifndef OCTAVE_CONF_CHOLMOD_LIBS
#define OCTAVE_CONF_CHOLMOD_LIBS "-lcholmod"
#endif

#ifndef OCTAVE_CONF_COLAMD_CPPFLAGS
#define OCTAVE_CONF_COLAMD_CPPFLAGS ""
#endif

#ifndef OCTAVE_CONF_COLAMD_LDFLAGS
#define OCTAVE_CONF_COLAMD_LDFLAGS ""
#endif

#ifndef OCTAVE_CONF_COLAMD_LIBS
#define OCTAVE_CONF_COLAMD_LIBS "-lcolamd"
#endif

#ifndef OCTAVE_CONF_CPICFLAG
#define OCTAVE_CONF_CPICFLAG "-fPIC"
#endif

#ifndef OCTAVE_CONF_CPPFLAGS
#define OCTAVE_CONF_CPPFLAGS "-Wdate-time -D_FORTIFY_SOURCE=2"
#endif

#ifndef OCTAVE_CONF_CURL_CPPFLAGS
#define OCTAVE_CONF_CURL_CPPFLAGS ""
#endif

#ifndef OCTAVE_CONF_CURL_LDFLAGS
#define OCTAVE_CONF_CURL_LDFLAGS ""
#endif

#ifndef OCTAVE_CONF_CURL_LIBS
#define OCTAVE_CONF_CURL_LIBS "-lcurl"
#endif

#ifndef OCTAVE_CONF_CXSPARSE_CPPFLAGS
#define OCTAVE_CONF_CXSPARSE_CPPFLAGS ""
#endif

#ifndef OCTAVE_CONF_CXSPARSE_LDFLAGS
#define OCTAVE_CONF_CXSPARSE_LDFLAGS ""
#endif

#ifndef OCTAVE_CONF_CXSPARSE_LIBS
#define OCTAVE_CONF_CXSPARSE_LIBS "-lcxsparse"
#endif

#ifndef OCTAVE_CONF_CXXCPP
#define OCTAVE_CONF_CXXCPP "g++ -E"
#endif

#ifndef OCTAVE_CONF_CXXFLAGS
#define OCTAVE_CONF_CXXFLAGS "-g -O2 -fdebug-prefix-map=/build/octave-tfO910/octave-4.0.3=. -fstack-protector-strong -Wformat -Werror=format-security"
#endif

#ifndef OCTAVE_CONF_CXXPICFLAG
#define OCTAVE_CONF_CXXPICFLAG "-fPIC"
#endif

#ifndef OCTAVE_CONF_CXX
#define OCTAVE_CONF_CXX "g++"
#endif

// FIXME: OCTAVE_CONF_CXX_VERSION is deprecated.  Remove in version 3.12
#ifndef OCTAVE_CONF_CXX_VERSION
#define OCTAVE_CONF_CXX_VERSION ""
#endif

#ifndef OCTAVE_CONF_DEFAULT_PAGER
#define OCTAVE_CONF_DEFAULT_PAGER "less"
#endif

#ifndef OCTAVE_CONF_DEFS
#define OCTAVE_CONF_DEFS "-DHAVE_CONFIG_H"
#endif

#ifndef OCTAVE_CONF_DL_LD
#define OCTAVE_CONF_DL_LD "g++"
#endif

#ifndef OCTAVE_CONF_DL_LDFLAGS
#define OCTAVE_CONF_DL_LDFLAGS "-shared"
#endif

#ifndef OCTAVE_CONF_DL_LIBS
#define OCTAVE_CONF_DL_LIBS "-ldl"
#endif

#ifndef OCTAVE_CONF_ENABLE_DYNAMIC_LINKING
#define OCTAVE_CONF_ENABLE_DYNAMIC_LINKING "yes"
#endif

#ifndef OCTAVE_CONF_EXEEXT
#define OCTAVE_CONF_EXEEXT ""
#endif

#ifndef OCTAVE_CONF_GCC_VERSION
#define OCTAVE_CONF_GCC_VERSION "6.2.1"
#endif

#ifndef OCTAVE_CONF_GXX_VERSION
#define OCTAVE_CONF_GXX_VERSION "6.2.1"
#endif

#ifndef OCTAVE_CONF_F77
#define OCTAVE_CONF_F77 "gfortran"
#endif

#ifndef OCTAVE_CONF_F77_FLOAT_STORE_FLAG
#define OCTAVE_CONF_F77_FLOAT_STORE_FLAG "-ffloat-store"
#endif

#ifndef OCTAVE_CONF_F77_INTEGER_8_FLAG
#define OCTAVE_CONF_F77_INTEGER_8_FLAG ""
#endif

#ifndef OCTAVE_CONF_FC
#define OCTAVE_CONF_FC %OCTAVE_CONF_FC%
#endif

#ifndef OCTAVE_CONF_FFLAGS
#define OCTAVE_CONF_FFLAGS "-g -O2 -fdebug-prefix-map=/build/octave-tfO910/octave-4.0.3=. -fstack-protector-strong"
#endif

#ifndef OCTAVE_CONF_FFTW3_CPPFLAGSS
#define OCTAVE_CONF_FFTW3_CPPFLAGS ""
#endif

#ifndef OCTAVE_CONF_FFTW3_LDFLAGSS
#define OCTAVE_CONF_FFTW3_LDFLAGS ""
#endif

#ifndef OCTAVE_CONF_FFTW3_LIBS
#define OCTAVE_CONF_FFTW3_LIBS "-lfftw3_threads -lfftw3"
#endif

#ifndef OCTAVE_CONF_FFTW3F_CPPFLAGSS
#define OCTAVE_CONF_FFTW3F_CPPFLAGS ""
#endif

#ifndef OCTAVE_CONF_FFTW3F_LDFLAGSS
#define OCTAVE_CONF_FFTW3F_LDFLAGS ""
#endif

#ifndef OCTAVE_CONF_FFTW3F_LIBS
#define OCTAVE_CONF_FFTW3F_LIBS "-lfftw3f_threads -lfftw3f"
#endif

#ifndef OCTAVE_CONF_FLIBS
#define OCTAVE_CONF_FLIBS "-L/usr/lib/gcc/arm-linux-gnueabihf/6 -L/usr/lib/gcc/arm-linux-gnueabihf/6/../../../arm-linux-gnueabihf -L/usr/lib/gcc/arm-linux-gnueabihf/6/../../.. -L/lib/arm-linux-gnueabihf -L/usr/lib/arm-linux-gnueabihf -lgfortran -lm"
#endif

#ifndef OCTAVE_CONF_FLTK_CPPFLAGS
#define OCTAVE_CONF_FLTK_CPPFLAGS "-I/usr/include/cairo -I/usr/include/glib-2.0 -I/usr/lib/arm-linux-gnueabihf/glib-2.0/include -I/usr/include/pixman-1 -I/usr/include/freetype2 -I/usr/include/libpng12 -I/usr/include/freetype2 -DCP936  -D_LARGEFILE_SOURCE -D_LARGEFILE64_SOURCE -D_FILE_OFFSET_BITS=64 -D_THREAD_SAFE -D_REENTRANT"
#endif

#ifndef OCTAVE_CONF_FLTK_LDFLAGS
#define OCTAVE_CONF_FLTK_LDFLAGS "-lfltk_gl -lfltk -lX11"
#endif

#ifndef OCTAVE_CONF_FLTK_LIBS
#define OCTAVE_CONF_FLTK_LIBS ""
#endif

#ifndef OCTAVE_CONF_FONTCONFIG_CPPFLAGS
#define OCTAVE_CONF_FONTCONFIG_CPPFLAGS "-I/usr/include/freetype2"
#endif

#ifndef OCTAVE_CONF_FONTCONFIG_LIBS
#define OCTAVE_CONF_FONTCONFIG_LIBS "-lfontconfig -lfreetype"
#endif

#ifndef OCTAVE_CONF_FPICFLAG
#define OCTAVE_CONF_FPICFLAG "-fPIC"
#endif

#ifndef OCTAVE_CONF_FT2_CPPFLAGS
#define OCTAVE_CONF_FT2_CPPFLAGS "-I/usr/include/freetype2"
#endif

#ifndef OCTAVE_CONF_FT2_LIBS
#define OCTAVE_CONF_FT2_LIBS "-lfreetype"
#endif

#ifndef OCTAVE_CONF_GLPK_CPPFLAGS
#define OCTAVE_CONF_GLPK_CPPFLAGS ""
#endif

#ifndef OCTAVE_CONF_GLPK_LDFLAGS
#define OCTAVE_CONF_GLPK_LDFLAGS ""
#endif

#ifndef OCTAVE_CONF_GLPK_LIBS
#define OCTAVE_CONF_GLPK_LIBS "-lglpk"
#endif

#ifndef OCTAVE_CONF_GNUPLOT
#define OCTAVE_CONF_GNUPLOT "gnuplot"
#endif

#ifndef OCTAVE_CONF_HDF5_CPPFLAGS
#define OCTAVE_CONF_HDF5_CPPFLAGS "-I/usr/include/hdf5/serial"
#endif

#ifndef OCTAVE_CONF_HDF5_LDFLAGS
#define OCTAVE_CONF_HDF5_LDFLAGS "-L/usr/lib/arm-linux-gnueabihf/hdf5/serial"
#endif

#ifndef OCTAVE_CONF_HDF5_LIBS
#define OCTAVE_CONF_HDF5_LIBS "-lhdf5"
#endif

#ifndef OCTAVE_CONF_INCFLAGS
#define OCTAVE_CONF_INCFLAGS %OCTAVE_CONF_INCFLAGS%
#endif

#ifndef OCTAVE_CONF_INCLUDEDIR
#define OCTAVE_CONF_INCLUDEDIR "/usr/include"
#endif

#ifndef OCTAVE_CONF_LAPACK_LIBS
#define OCTAVE_CONF_LAPACK_LIBS "-llapack"
#endif

#ifndef OCTAVE_CONF_LDFLAGS
#define OCTAVE_CONF_LDFLAGS "-Wl,-z,relro"
#endif

#ifndef OCTAVE_CONF_LD_CXX
#define OCTAVE_CONF_LD_CXX "g++"
#endif

#ifndef OCTAVE_CONF_LD_STATIC_FLAG
#define OCTAVE_CONF_LD_STATIC_FLAG ""
#endif

#ifndef OCTAVE_CONF_LEXLIB
#define OCTAVE_CONF_LEXLIB ""
#endif

#ifndef OCTAVE_CONF_LEX
#define OCTAVE_CONF_LEX "flex"
#endif

#ifndef OCTAVE_CONF_LFLAGS
#define OCTAVE_CONF_LFLAGS "-I"
#endif

#ifndef OCTAVE_CONF_LIBEXT
#define OCTAVE_CONF_LIBEXT "a"
#endif

#ifndef OCTAVE_CONF_LIBFLAGS
#define OCTAVE_CONF_LIBFLAGS "-L.."
#endif

#ifndef OCTAVE_CONF_LIBOCTAVE
#define OCTAVE_CONF_LIBOCTAVE "-loctave"
#endif

#ifndef OCTAVE_CONF_LIBOCTINTERP
#define OCTAVE_CONF_LIBOCTINTERP "-loctinterp"
#endif

#ifndef OCTAVE_CONF_LIBS
#define OCTAVE_CONF_LIBS "-lutil -lm  "
#endif

#ifndef OCTAVE_CONF_LN_S
#define OCTAVE_CONF_LN_S "ln -s"
#endif

#ifndef OCTAVE_CONF_MAGICK_CPPFLAGS
#define OCTAVE_CONF_MAGICK_CPPFLAGS "-I/usr/include/GraphicsMagick"
#endif

#ifndef OCTAVE_CONF_MAGICK_LDFLAGS
#define OCTAVE_CONF_MAGICK_LDFLAGS ""
#endif

#ifndef OCTAVE_CONF_MAGICK_LIBS
#define OCTAVE_CONF_MAGICK_LIBS "-lGraphicsMagick++ -lGraphicsMagick"
#endif

#ifndef OCTAVE_CONF_LLVM_CPPFLAGS
#define OCTAVE_CONF_LLVM_CPPFLAGS ""
#endif

#ifndef OCTAVE_CONF_LLVM_LDFLAGS
#define OCTAVE_CONF_LLVM_LDFLAGS ""
#endif

#ifndef OCTAVE_CONF_LLVM_LIBS
#define OCTAVE_CONF_LLVM_LIBS ""
#endif

#ifndef OCTAVE_CONF_MKOCTFILE_DL_LDFLAGS
#define OCTAVE_CONF_MKOCTFILE_DL_LDFLAGS "-shared -Wl,-Bsymbolic"
#endif

#ifndef OCTAVE_CONF_OCTAVE_LINK_DEPS
#define OCTAVE_CONF_OCTAVE_LINK_DEPS ""
#endif

#ifndef OCTAVE_CONF_OCTAVE_LINK_OPTS
#define OCTAVE_CONF_OCTAVE_LINK_OPTS ""
#endif

#ifndef OCTAVE_CONF_OCTINCLUDEDIR
#define OCTAVE_CONF_OCTINCLUDEDIR "/usr/include/octave-4.0.3/octave"
#endif

#ifndef OCTAVE_CONF_OCTLIBDIR
#define OCTAVE_CONF_OCTLIBDIR "/usr/lib/arm-linux-gnueabihf/octave/4.0.3"
#endif

#ifndef OCTAVE_CONF_OCT_LINK_DEPS
#define OCTAVE_CONF_OCT_LINK_DEPS ""
#endif

#ifndef OCTAVE_CONF_OCT_LINK_OPTS
#define OCTAVE_CONF_OCT_LINK_OPTS "-Wl,-z,relro "
#endif

#ifndef OCTAVE_CONF_OPENGL_LIBS
#define OCTAVE_CONF_OPENGL_LIBS "-lfontconfig -lfreetype -lGL -lGLU"
#endif

#ifndef OCTAVE_CONF_OSMESA_CPPFLAGS
#define OCTAVE_CONF_OSMESA_CPPFLAGS ""
#endif

#ifndef OCTAVE_CONF_OSMESA_LDFLAGS
#define OCTAVE_CONF_OSMESA_LDFLAGS ""
#endif

#ifndef OCTAVE_CONF_OSMESA_LIBS
#define OCTAVE_CONF_OSMESA_LIBS "-lOSMesa"
#endif

#ifndef OCTAVE_CONF_PCRE_CPPFLAGS
#define OCTAVE_CONF_PCRE_CPPFLAGS ""
#endif

#ifndef OCTAVE_CONF_PCRE_LIBS
#define OCTAVE_CONF_PCRE_LIBS "-lpcre"
#endif

#ifndef OCTAVE_CONF_PREFIX
#define OCTAVE_CONF_PREFIX "/usr"
#endif

#ifndef OCTAVE_CONF_PTHREAD_CFLAGS
#define OCTAVE_CONF_PTHREAD_CFLAGS "-pthread"
#endif

#ifndef OCTAVE_CONF_PTHREAD_LIBS
#define OCTAVE_CONF_PTHREAD_LIBS ""
#endif

#ifndef OCTAVE_CONF_QHULL_CPPFLAGS
#define OCTAVE_CONF_QHULL_CPPFLAGS ""
#endif

#ifndef OCTAVE_CONF_QHULL_LDFLAGS
#define OCTAVE_CONF_QHULL_LDFLAGS ""
#endif

#ifndef OCTAVE_CONF_QHULL_LIBS
#define OCTAVE_CONF_QHULL_LIBS "-lqhull"
#endif

#ifndef OCTAVE_CONF_QRUPDATE_CPPFLAGS
#define OCTAVE_CONF_QRUPDATE_CPPFLAGS ""
#endif

#ifndef OCTAVE_CONF_QRUPDATE_LDFLAGS
#define OCTAVE_CONF_QRUPDATE_LDFLAGS ""
#endif

#ifndef OCTAVE_CONF_QRUPDATE_LIBS
#define OCTAVE_CONF_QRUPDATE_LIBS "-lqrupdate"
#endif

#ifndef OCTAVE_CONF_QT_CPPFLAGS
#define OCTAVE_CONF_QT_CPPFLAGS "-I/usr/include/qt4 -I/usr/include/qt4/QtNetwork -I/usr/include/qt4 -I/usr/include/qt4/QtOpenGL -I/usr/include/qt4 -I/usr/include/qt4/QtGui -I/usr/include/qt4 -I/usr/include/qt4/QtCore"
#endif

#ifndef OCTAVE_CONF_QT_LDFLAGS
#define OCTAVE_CONF_QT_LDFLAGS ""
#endif

#ifndef OCTAVE_CONF_QT_LIBS
#define OCTAVE_CONF_QT_LIBS "-lQtNetwork -lQtOpenGL -lQtGui -lQtCore -lqscintilla2"
#endif

#ifndef OCTAVE_CONF_RANLIB
#define OCTAVE_CONF_RANLIB "ranlib"
#endif

#ifndef OCTAVE_CONF_RDYNAMIC_FLAG
#define OCTAVE_CONF_RDYNAMIC_FLAG "-rdynamic"
#endif

#ifndef OCTAVE_CONF_READLINE_LIBS
#define OCTAVE_CONF_READLINE_LIBS "-lreadline"
#endif

#ifndef OCTAVE_CONF_SED
#define OCTAVE_CONF_SED "/bin/sed"
#endif

#ifndef OCTAVE_CONF_SHARED_LIBS
#define OCTAVE_CONF_SHARED_LIBS ""
#endif

#ifndef OCTAVE_CONF_SHLEXT
#define OCTAVE_CONF_SHLEXT "so"
#endif

#ifndef OCTAVE_CONF_SHLEXT_VER
#define OCTAVE_CONF_SHLEXT_VER "so.4.0.3"
#endif

#ifndef OCTAVE_CONF_SH_LD
#define OCTAVE_CONF_SH_LD "g++"
#endif

#ifndef OCTAVE_CONF_SH_LDFLAGS
#define OCTAVE_CONF_SH_LDFLAGS "-shared"
#endif

#ifndef OCTAVE_CONF_SONAME_FLAGS
#define OCTAVE_CONF_SONAME_FLAGS "-Wl,-soname -Wl,oct-conf.h"
#endif

#ifndef OCTAVE_CONF_STATIC_LIBS
#define OCTAVE_CONF_STATIC_LIBS ""
#endif

#ifndef OCTAVE_CONF_TERM_LIBS
#define OCTAVE_CONF_TERM_LIBS "-lncurses"
#endif

#ifndef OCTAVE_CONF_UMFPACK_CPPFLAGS
#define OCTAVE_CONF_UMFPACK_CPPFLAGS ""
#endif

#ifndef OCTAVE_CONF_UMFPACK_LDFLAGS
#define OCTAVE_CONF_UMFPACK_LDFLAGS ""
#endif

#ifndef OCTAVE_CONF_UMFPACK_LIBS
#define OCTAVE_CONF_UMFPACK_LIBS "-lumfpack"
#endif

#ifndef OCTAVE_CONF_USE_64_BIT_IDX_T
#define OCTAVE_CONF_USE_64_BIT_IDX_T "no"
#endif

#ifndef OCTAVE_CONF_WARN_CFLAGS
#define OCTAVE_CONF_WARN_CFLAGS "-Wall -W -Wshadow -Wformat -Wpointer-arith -Wmissing-prototypes -Wstrict-prototypes -Wwrite-strings -Wcast-align -Wcast-qual"
#endif

#ifndef OCTAVE_CONF_WARN_CXXFLAGS
#define OCTAVE_CONF_WARN_CXXFLAGS "-Wall -W -Wshadow -Wold-style-cast -Wformat -Wpointer-arith -Wwrite-strings -Wcast-align -Wcast-qual"
#endif

#ifndef OCTAVE_CONF_X11_INCFLAGS
#define OCTAVE_CONF_X11_INCFLAGS ""
#endif

#ifndef OCTAVE_CONF_X11_LIBS
#define OCTAVE_CONF_X11_LIBS "-lX11"
#endif

#ifndef OCTAVE_CONF_XTRA_CFLAGS
#define OCTAVE_CONF_XTRA_CFLAGS "-pthread -fopenmp"
#endif

#ifndef OCTAVE_CONF_XTRA_CXXFLAGS
#define OCTAVE_CONF_XTRA_CXXFLAGS "-pthread -fopenmp"
#endif

#ifndef OCTAVE_CONF_YACC
#define OCTAVE_CONF_YACC "../build-aux/missing bison"
#endif

#ifndef OCTAVE_CONF_YFLAGS
#define OCTAVE_CONF_YFLAGS ""
#endif

#ifndef OCTAVE_CONF_Z_CPPFLAGS
#define OCTAVE_CONF_Z_CPPFLAGS ""
#endif

#ifndef OCTAVE_CONF_Z_LDFLAGS
#define OCTAVE_CONF_Z_LDFLAGS ""
#endif

#ifndef OCTAVE_CONF_Z_LIBS
#define OCTAVE_CONF_Z_LIBS "-lz"
#endif

#ifndef OCTAVE_CONF_config_opts
#define OCTAVE_CONF_config_opts "'--build=arm-linux-gnueabihf' '--prefix=/usr' '--includedir=/usr/include' '--mandir=/usr/share/man' '--infodir=/usr/share/info' '--sysconfdir=/etc' '--localstatedir=/var' '--disable-silent-rules' '--libdir=/usr/lib/arm-linux-gnueabihf' '--libexecdir=/usr/lib/arm-linux-gnueabihf' '--disable-maintainer-mode' '--disable-dependency-tracking' '--enable-openmp' '--with-java-homedir=/usr/lib/jvm/default-java' '--with-java-libdir=/usr/lib/jvm/default-java/jre/lib/arm/server' '--disable-jit' '--with-hdf5-includedir=/usr/include/hdf5/serial' '--with-hdf5-libdir=/usr/lib/arm-linux-gnueabihf/hdf5/serial' 'build_alias=arm-linux-gnueabihf' 'CFLAGS=-g -O2 -fdebug-prefix-map=/build/octave-tfO910/octave-4.0.3=. -fstack-protector-strong -Wformat -Werror=format-security' 'LDFLAGS=-Wl,-z,relro' 'CPPFLAGS=-Wdate-time -D_FORTIFY_SOURCE=2' 'CXXFLAGS=-g -O2 -fdebug-prefix-map=/build/octave-tfO910/octave-4.0.3=. -fstack-protector-strong -Wformat -Werror=format-security' 'FFLAGS=-g -O2 -fdebug-prefix-map=/build/octave-tfO910/octave-4.0.3=. -fstack-protector-strong'"
#endif

#endif
