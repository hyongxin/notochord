/**
 * @file run_octave.h
 * Launch the octave interpreter and run a function
 *
 * @author Bruno Laurencich
 * @version 0.2.0 
 *
 * Notochord  
 * -- Hub program for the Chordata Open Source motion capture system
 *
 * http://chordata.cc
 * contact@chordata.cc
 *
 *
 * Copyright 2018 - 2020 Bruno Laurencich
 *
 * This file is part of Notochord.
 *
 * Notochord is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Notochord is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Notochord. If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include <array>
#include <vector>
#include <tuple>
#include <cstdint>
#include <string>

#include <oct.h>
#include <octave.h>
#include <parse.h>
#include <toplev.h> /* do_octave_atexit */
#include <iostream>

#include <limits>

#include "Notochord.h"
#include "Chordata_communicator.h"
namespace comm = Chordata::Communicator;

// g++ octave.cpp -o octave -L /usr/lib/x86_64-linux-gnu  -I /usr/include/octave-4.0.0/octave -l octinterp -l octave
// https://lists.gnu.org/archive/html/help-octave/2009-04/msg00005.html
// 
// https://stackoverflow.com/questions/9246444/how-to-embed-the-gnu-octave-in-c-c-program
// 
// https://www.gnu.org/software/octave/doc/interpreter/Standalone-Programs.html#Standalone-Programs

// std::tuple<int, int, int>
using calibration_offset = std::array<int16_t, 3>;
using calibration_matrix = std::array<float, 9>;
using octave_calib_result = std::tuple<calibration_offset, calibration_matrix>;  

octave_calib_result runOctave(const std::vector<std::array<int16_t, 3>>& lectures)
{
	const char * argvv [] = {"" /* name of program, not relevant */, "--silent"};

  octave_main (2, (char **) argvv, true /* embedded */);

  octave_value_list functionArguments;

  Matrix inMatrix (lectures.size(), 3);

  for (int i =0; i< lectures.size(); ++i){
    
    inMatrix (i, 0) = lectures[i][0];
    inMatrix (i, 1) = lectures[i][1];
    inMatrix (i, 2) = lectures[i][2];

  }

  int status = 0;
  std::string cd = "cd ";
  cd += Chordata::getConf().exe_path;
  eval_string(cd.c_str(), true, status);
  

  functionArguments (0) = inMatrix;
  comm::info("please wait while the data is processed.. (might take some seconds)\n" _CHORDATA_DEF_EOM );

  const octave_value_list result = feval ("getMagCalib", functionArguments, 1);

  // std::cout << "pausing Octave interpreter.. press a key to continue\n" _CHORDATA_DEF_PROMPT << std::endl;

  // eval_string("pause", true, status);

  // std::cout << "unpaused" << std::endl;
  // std::cin.ignore();

  // t.join();
  // std::cout << "result is " << result(0).vector_value () << std::endl;
  // std::cout << "elements " << result.length() << std::endl;
  
  // std::cout << "resultString is " << result (1).string_value () << std::endl;
  // std::cout << "resultMatrix is\n" << result (2).matrix_value ();

  auto vValue = result(1).vector_value();
  auto mValue = result(2).matrix_value();

  for (int i = 0; i < 3; ++i)
  {
   if (std::numeric_limits<int16_t>::min() > vValue(i) ||
        std::numeric_limits<int16_t>::max() < vValue(i)){
        comm::err( "One of the values is out of the bounds of a 16bit int, overflow will cause data corruption");
        clean_up_and_exit(0);
      }
  }
  calibration_offset vArray = {vValue(0), vValue(1), vValue(2)}; 

  calibration_matrix mArray = {mValue(0,0), mValue(0,1), mValue(0,2),
                               mValue(1,0), mValue(1,1), mValue(1,2),
                               mValue(2,0), mValue(2,1), mValue(2,2)};


	return std::make_tuple(std::move(vArray), std::move(mArray));
}