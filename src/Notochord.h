/**
 * @file Notochord.h
 * Header for the entry point of the program
 *
 * @author Bruno Laurencich
 * @version 0.2.0 
 *
 * Notochord  
 * -- Hub program for the Chordata Open Source motion capture system
 *
 * http://chordata.cc
 * contact@chordata.cc
 *
 *
 * Copyright 2018 - 2020 Bruno Laurencich
 *
 * This file is part of Notochord.
 *
 * Notochord is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Notochord is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Notochord. If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef	__NOTOCHORD__
#define __NOTOCHORD__

#include <vector>
#include <mutex>
#include <thread>
#include <functional>
#include <condition_variable>
#include "Chordata_def.h"
#include "Chordata_utils.h"
#include "Chordata_parser.h"
#include "Chordata_timer.h"
#include "Chordata_node.h"
#include "Chordata_circular_buffer.h"
#include "Chordata_communicator.h"
#include "Chordata_imu.h"
#include "../test/Chordata_test.h"


namespace Chordata{
	// typedef std::function<const Chordata::Configuration_Data&(void)> config_fn;

	// extern config_fn getConf;

	class Notochord{
	private:
		friend Chordata_test::Test_Helper;
		typedef Timer<Node_Scheluder> Timer_Sch;
		using lock = std::lock_guard<std::mutex>;
		
		std::unique_ptr<Armature_Parser> aParser;
		const Chordata::Configuration_Data config;
		
		std::mutex timer_lock;
		Node_Scheluder scheluder;
		Timer_Sch main_timer;
		
		I2C_io *i2c;
		// I_Imu<I2C_io> * imu;
		std::unique_ptr<Armature> armature;
		
		std::mutex workers_lock;
		std::vector<std::thread> workers;
		std::condition_variable whistle;
		int objeto;

		int spawn_threads();

		int join_threads();

		int inform_thread(int n);
	
	public:		
		Notochord(const Notochord&) = delete;
		Notochord& operator=(const Notochord&) = delete;

		Notochord(Notochord&&) = delete;

		explicit Notochord(Chordata::Configuration_Data&& = createDefaultConfig());
		
		Notochord(int argc, const char **argv);

		const Chordata::Configuration_Data& getConf() const noexcept{
			return config;
		}

		void init_communicator();

		bool initI2C();

		bool createArmature();

		bool scanArmature();

		bool run();

		void stop();


	};
}//namespace Chordata
#endif

