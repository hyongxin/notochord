/**
 * @file Chordata_timer.cpp
 * Time-handlind classes: Scheduler to organize the reading of the nodes, and Timer to launch each read. 
 *
 * @author Bruno Laurencich
 * @version 0.2.0 
 *
 * Notochord  
 * -- Hub program for the Chordata Open Source motion capture system
 *
 * http://chordata.cc
 * contact@chordata.cc
 *
 *
 * Copyright 2018 - 2020 Bruno Laurencich
 *
 * This file is part of Notochord.
 *
 * Notochord is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Notochord is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Notochord. If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include "Chordata_communicator.h"
#include "Chordata_timer.h"
#include "Chordata_def.h"
#include "Chordata_utils.h"
#include "Chordata_node.h"
#include <iostream>

namespace comm = Chordata::Communicator;

using namespace std;
using namespace Chordata;
using scheduler_citer = std::vector<I2C_Node*>::const_iterator;

#define LAP_MODIF_RATE 10
#define LAP_TOLERANCE 100
#define LAP_CORRECT_TIME_MARGIN 10*1000

void Node_Scheluder::addNode(I2C_Node *n){
		nodelist.push_back(n);
		current = nodelist.end()-1;

		if (n->isSensor())
			ODRModifiers++;
		
	};

const scheduler_citer seek_first_sensor(const std::vector<I2C_Node*>& list){
	for (scheduler_citer i = list.begin(); i != list.end(); ++i){
		if ((*i)->isSensor()) return i;
	}
}


long int Node_Scheluder::bang(long int wait){
	//not threaded at the moment
	// thread nodeSearch(&Node_Scheluder::doSearch, this);
	// nodeSearch.detach();
	
	static long int lap_teoric_duration = 1000000 / Chordata::getConf().odr;
	static long int teoric_wait = lap_teoric_duration / getODRModifiers();
	static auto first_sensor = seek_first_sensor(nodelist);
	static int counter = Chordata::getConf().odr;
	
	// Chordata::K_Ceptor* k  = dynamic_cast< Chordata::K_Ceptor*>(*current);
	// if (k) comm::info("CURRENT: {}", k->getLabel());
	
	if (current == first_sensor){
		comm::transmit_bundles(getODRModifiers());
		// comm::info("FIRST: {}", k->getLabel());

		//Check if lap duration is correct and adjust;
		auto lap  = comm::timekeeper.ellapsedLapMicros();
		if (counter < 1){
			counter = Chordata::getConf().odr;
			comm::trace(" ===== Ellapsed Lap Micros: {} =====", lap);
			for ( I2C_Node* n : nodelist) { 
				if ( Chordata::K_Ceptor* kc  = dynamic_cast< Chordata::K_Ceptor*>(n)){
					comm::trace("Node {:>30} lap: {:10d}", kc->getLabel(), kc->ellapsedLapMicros());
					
					if (!Chordata::getConf().fusion.madgwick){
						comm::transmit(fmt::format("/Chordata/extra/{}/amcov",kc->getLabel() ), 
							kc->get_kalman().get_a_covariance(), 
							kc->get_kalman().get_m_covariance());

					}
				}

			}
			
		}
		counter --;
		comm::timekeeper.resetLap();
		
		if (comm::timekeeper.ellapsedMillis() > LAP_CORRECT_TIME_MARGIN ){
			//Leave the first second of execution untouched
			//after that start cheking if the lap has to be increased or decreased
			
			if (lap > lap_teoric_duration + LAP_TOLERANCE){
				wait -= LAP_MODIF_RATE;
				//comm::debug(" (- ) Read wait reduced, now is {} (teoric = {}, [Lap measured: {}, Lap teoric: {}])", 
				//	wait, teoric_wait,
				//	lap, lap_teoric_duration);
			} else if (lap < lap_teoric_duration - LAP_TOLERANCE){
				wait += LAP_MODIF_RATE;
				//comm::debug(" ( +) Read wait increased, now is {} (teoric = {}, [Lap measured: {}, Lap teoric: {}])", 
				//	wait, teoric_wait,
				//	lap, lap_teoric_duration);
					
			}
		}
	} // End lap duration cheking
	
	doSearch();
	return wait;
}			

void Node_Scheluder::doSearch(){
	//not threaded at the moment
	// mp->lock();
		// cout << "LABEL: " << (*current)->getLabel() << endl;
		main_buffer.put(*current);
	// mp->unlock();
	step();

	while (!(*current)->isSensor()){
		main_buffer.put(*current);
		step();
	}

	//TODO: at this point inform another thread, and it will "eat" the nodes in the buffer.
	//This other thread will be responsable for checking eventual throws of the banged node.
	main_buffer.eat();
	
}


////////////////////////////////// 
/// for testing individual utils
//////////////////////////////////

#ifdef __TEST_TIMER__
int main(int argc, char const *argv[])
{	
	typedef Timer<Node_Scheluder> Timer_Sch;
	
	Node_Scheluder n;
	mutex *m = n.getMutex();
	Timer_Sch t(&n, 50000, m);

	// thread contador(&Timer_Sch::startTimer, t );
	thread contador([&]{t.startTimer();} );
	

	// while(1){
	// 		m->lock();
	// 			cout<< "MAIN THREAD MUTEX: " << m << endl;
	// 		m->unlock();
	// 		thread_sleep(micros(1000000));
	// }

	if(contador.joinable())
		contador.join();

	return 0;
}

#endif
