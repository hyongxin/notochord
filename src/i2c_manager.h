/**
 * @file i2c_manager.h
 * Handler classes for Linux i2c adapter
 *
 * @author Bruno Laurencich
 * @version 0.2.0 
 *
 * Notochord  
 * -- Hub program for the Chordata Open Source motion capture system
 *
 * http://chordata.cc
 * contact@chordata.cc
 *
 *
 * Copyright 2018 - 2020 Bruno Laurencich
 *
 * This file is part of Notochord.
 *
 * Notochord is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Notochord is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Notochord. If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef __I2C_MANAGER_H_
#define __I2C_MANAGER_H_

#include "Chordata_def.h"

#include <cstdint>
#include <stdio.h>
#include <string>

#include <iostream>


/**
 *  Represents a i2c hardware device handled from user space through an adapter
 *  
 */


class I2C_Device{
	int i2c_file;
	std::string i2c_adapter;

public:
	bool ready() const { return i2c_file > 0; }

	explicit I2C_Device(const std::string& adapter = _CHORDATA_I2C_DEFAULT_ADAPTER_, const bool doInit = true);

	I2C_Device (const I2C_Device& ) = delete;
	I2C_Device& operator=(const I2C_Device&) = delete;
	I2C_Device (I2C_Device&& ) = default;
	I2C_Device& operator=(I2C_Device&&) = default;

	int get_i2c_file() const{
		return i2c_file;
	}

	Chordata::i2c_result init();
};

#ifdef _CHORDATA_TEST_BUILD__
#define _VIRTUAL_FOR_TEST_ virtual
#else
#define _VIRTUAL_FOR_TEST_ 
#endif

class I2C_io {
	I2C_Device *adapter;

public:
	bool ready() const { return adapter->ready(); }

	explicit I2C_io(I2C_Device *_adapter):
	adapter(_adapter)
	{};

	int get_adapter_i2c_file() const{
		return adapter->get_i2c_file();
	}

	// I2CwriteByte() -- Write a byte out of I2C directly into the device (no register set)
	// Input:
	//	- address = The 7-bit I2C address of the slave device.
	//	- data = Byte to be written to the register.
	_VIRTUAL_FOR_TEST_ void I2CwriteByte(uint8_t address, uint8_t data);

	inline void writeByte(uint8_t address, uint8_t data){
		I2CwriteByte(address, data);
	}


	// I2CwriteByte() -- Write a byte out of I2C to a register in the device
	// Input:
	//	- address = The 7-bit I2C address of the slave device.
	//	- subAddress = The register to be written to.
	//	- data = Byte to be written to the register.
	_VIRTUAL_FOR_TEST_ void I2CwriteByte(uint8_t address, uint8_t subAddress, uint8_t data);
	
	inline void writeByte(uint8_t address, uint8_t subAddress, uint8_t data){
		I2CwriteByte(address, subAddress, data);
	}

	// I2CreadByte() -- Read a single byte from a register over I2C.
	// Input:
	//	- address = The 7-bit I2C address of the slave device.
	//	- subAddress = The register to be read from.
	// Output:
	//	- The byte read from the requested address. (returned as a 32bit signed in order to match the SMBus return type)
	_VIRTUAL_FOR_TEST_ int32_t I2CreadByte(uint8_t address, uint8_t subAddress);
	
	inline int32_t readByte(uint8_t address, uint8_t subAddress){
		return I2CreadByte(address, subAddress);
	}
	
	// I2CreadBytes() -- Read a series of bytes, starting at a register via I2C
	// Input:
	//	- address = The 7-bit I2C address of the slave device.
	//	- subAddress = The register to begin reading.
	// 	- * dest = Pointer to an array where we'll store the readings.
	//	- count = Number of registers to be read.
	// Output: No value is returned by the function, but the registers read are
	// 		all stored in the *dest array given.
	_VIRTUAL_FOR_TEST_ void I2CreadBytes(uint8_t address, uint8_t subAddress, uint8_t * dest, uint8_t count);

	inline void readBytes(uint8_t address, uint8_t subAddress, uint8_t * dest, uint8_t count){
		I2CreadBytes(address, subAddress, dest, count);
	}

	
};

#endif